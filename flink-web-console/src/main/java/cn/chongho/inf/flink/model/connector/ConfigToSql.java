package cn.chongho.inf.flink.model.connector;

/**
 * @author ming
 */
public interface ConfigToSql {

    String doConfigToSql();
}
